<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Use Faker\Factory;

class Seeder extends CI_Controller {
    function __construct(){
        parent::__construct();

        $this->load->model('seeder_model', 'mdl');
    }

    function index(){
        $faker = Faker\Factory::create('id_ID');

        $result = array();

        for($i = 1; $i <= 100; $i++):

            $data = array(
                'produk'       => $faker->sentence(5),
                'deskripsi'    => $faker->sentence(100),
                'stok'         => $faker->randomNumber(3),
                'supplier_id'  => $faker->randomElement($array = array (1,2)),
                'kategori_id'  => $faker->randomElement($array = array (1,2,3,4))
            );

            array_push($result, $data);

            $this->mdl->simpan($data);
        endfor;

        echo '<pre>';print_r($result);
    }
}