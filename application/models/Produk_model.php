<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_model extends CI_Model {

    protected $table = 'produk';

    function get_all(){
    return $this->db->get($this->table);
  }
  
    function get_by_id($id){

      return $this->db->get_where($this->table, array('produk_id' => $id));
    }

    function simpan($data){
        $this->db->insert($this->table, $data);
    }

    function perbaharui($data, $key){
      $this->db->update($this->table, $data, $key);
    }

    function hapus($key){
      $this->db->delete($this->table, $key);
    }

    function total_data(){
      return $this->count_all($this->table);
    }
}