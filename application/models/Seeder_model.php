<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seeder_model extends CI_Model {
    protected $table;

    function __construct(){
        $this->table = 'produk';
        }

        function simpan($data){
            $this->db->insert($this->table, $data);
        }
}